import 'dart:async';
import 'package:flutter/material.dart';
import 'package:foodpanda_sellers_app/authentication/authentication_page.dart';
import 'package:foodpanda_sellers_app/global/global.dart';
import 'package:foodpanda_sellers_app/main/home_page.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  startTimer() async {
    Timer(const Duration(seconds: 4), () async {
      final g = await Global.getInstance();
      Widget page = g.firebaseAuth.currentUser == null ? const AuthenticationPage() : const HomePage();
      Navigator.push(context, MaterialPageRoute(builder: (c) => page));
    });
  }

  @override
  void initState() {
    super.initState();

    startTimer();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
          color: Colors.white,
          child: FittedBox(
            fit: BoxFit.scaleDown,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(18.0),
                    child: Image.asset('images/splash.jpg'),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      "Sell Food Online",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.black54,
                        fontSize: 40,
                        fontFamily: 'Signatra',
                        letterSpacing: 3,
                      ),
                    ),
                  )
                ],
              ),
            ),
          )),
    );
  }
}
