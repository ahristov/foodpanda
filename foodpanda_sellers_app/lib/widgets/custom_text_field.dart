import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  final TextEditingController? controller;
  final IconData? iconData;
  final String? hintText;
  final bool? isObscure;
  final bool? enabled;

  const CustomTextField({
    Key? key,
    this.controller,
    this.iconData,
    this.hintText,
    this.isObscure = false,
    this.enabled = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        padding: const EdgeInsets.all(8.0),
        margin: const EdgeInsets.all(10.0),
        child: TextFormField(
          enabled: enabled,
          controller: controller,
          obscureText: isObscure ?? false,
          cursorColor: Theme.of(context).primaryColor,
          decoration: InputDecoration(
            border: InputBorder.none,
            prefixIcon: Icon(
              iconData,
              color: Colors.cyan,
            ),
            focusColor: Theme.of(context).primaryColor,
            hintText: hintText,
          ),
        ));
  }
}
