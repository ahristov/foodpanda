import 'dart:async';
import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:foodpanda_users_app/global/global.dart';
import 'package:foodpanda_users_app/main/home_page.dart';
import 'package:foodpanda_users_app/widgets/custom_alert_dialog.dart';
import 'package:foodpanda_users_app/widgets/custom_text_field.dart';
import 'package:foodpanda_users_app/widgets/loading_dialog.dart';
import 'package:image_picker/image_picker.dart';

class RegisterView extends StatefulWidget {
  const RegisterView({Key? key}) : super(key: key);

  @override
  State<RegisterView> createState() => _RegisterViewState();
}

class _RegisterViewState extends State<RegisterView> {
  String _imageUrl = '';

  XFile? _imageXFile;
  final ImagePicker _imagePicker = ImagePicker();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Future<void> _getImage() async {
    if (Platform.isAndroid || Platform.isIOS) {
      _imageXFile = await _imagePicker.pickImage(source: ImageSource.gallery);
      setState(() {
        _imageXFile;
      });
    }
  }

  Future<void> _formValidation() async {
    if (_imageXFile == null) {
      showDialog(
        context: context,
        builder: (ctx) => const CustomAlertDialog.alert(message: 'Please select an image.'),
      );
      return;
    }

    if (_passwordController.text.isEmpty) {
      showDialog(
        context: context,
        builder: (ctx) => const CustomAlertDialog.alert(message: 'Please enter password.'),
      );
      return;
    }

    if (_passwordController.text != _confirmPasswordController.text) {
      showDialog(
        context: context,
        builder: (ctx) => const CustomAlertDialog.alert(message: 'Password do not match.'),
      );
      return;
    }

    if (_passwordController.text != _confirmPasswordController.text) {
      showDialog(
        context: context,
        builder: (ctx) => const CustomAlertDialog.alert(message: 'Password do not match.'),
      );
      return;
    }

    if (_nameController.text.isEmpty || _emailController.text.isEmpty) {
      showDialog(
        context: context,
        builder: (ctx) => const CustomAlertDialog.alert(message: 'Please enter the required info for registration.'),
      );
      return;
    }

    // start uploading the image
    showDialog(
      context: context,
      builder: (ctx) => const LoadingDialog(message: 'Registering account, please wait...'),
    );

    final fileName = DateTime.now().millisecondsSinceEpoch.toString();
    final g = await Global.getInstance();
    final reference = g.firebaseStorage.ref().child('users').child(fileName);
    final uploadTask = reference.putFile(File(_imageXFile!.path));
    final taskSnapshot = await uploadTask.whenComplete(() {});
    await taskSnapshot.ref.getDownloadURL().then((url) {
      _imageUrl = url;

      _authenticateAndSignUp();
    });

    return;
  }

  void _authenticateAndSignUp() async {
    User? currentUser;
    final g = await Global.getInstance();

    try {
      await g.firebaseAuth
          .createUserWithEmailAndPassword(
        email: _emailController.text.trim(),
        password: _passwordController.text.trim(),
      )
          .then((auth) {
        currentUser = auth.user;
      });
    } on FirebaseAuthException catch (error) {
      _onAuthenticateAndSignUpError(error.message);
      return;
    } catch (error) {
      _onAuthenticateAndSignUpError(error);
      return;
    }

    if (currentUser == null) {
      _onAuthenticateAndSignUpError('Error signing up.');
      return;
    }

    try {
      await _saveDataToFirestore(currentUser!).then((value) {
        _onAuthenticateAndSignUpSuccess();
      });
    } catch (error) {
      _onAuthenticateAndSignUpError(error);
    }
  }

  void _onAuthenticateAndSignUpError(dynamic error) {
    // close loading pop-up
    Navigator.pop(context);
    // show error
    showDialog(
      context: context,
      builder: (ctx) => CustomAlertDialog.alert(message: error.toString()),
    );
  }

  void _onAuthenticateAndSignUpSuccess() {
    // close loading pop-up
    Navigator.pop(context);

    // redirect to homepage
    Route newRoute = MaterialPageRoute(builder: (ctx) => const HomePage());
    Navigator.pushReplacement(context, newRoute);
  }

  Future _saveDataToFirestore(User currentUser) async {
    final g = await Global.getInstance();

    g.firebaseFirestore.collection('users').doc(currentUser.uid).set({
      'userUID': currentUser.uid,
      'userEmail': currentUser.email,
      'userName': _nameController.text.trim(),
      'userPhotoUrl': _imageUrl,
      'status': 'approved',
    });

    g.sharedPreferences.setString('uid', currentUser.uid);
    g.sharedPreferences.setString('name', _nameController.text.trim());
    g.sharedPreferences.setString('email', currentUser.email ?? '');
    g.sharedPreferences.setString('photoUrl', _imageUrl);
  }

  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _confirmPasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          const SizedBox(height: 10),
          InkWell(
            onTap: _getImage,
            child: CircleAvatar(
                radius: MediaQuery.of(context).size.width * 0.20,
                backgroundColor: Colors.white,
                backgroundImage: _imageXFile == null ? null : FileImage(File(_imageXFile!.path)),
                child: _imageXFile == null
                    ? Icon(
                        Icons.add_photo_alternate,
                        size: MediaQuery.of(context).size.width * 0.20,
                        color: Colors.grey,
                      )
                    : null),
          ),
          const SizedBox(height: 10),
          Form(
            key: _formKey,
            child: Column(
              children: [
                CustomTextField(controller: _nameController, iconData: Icons.person, hintText: 'Name'),
                CustomTextField(controller: _emailController, iconData: Icons.email, hintText: 'Email'),
                CustomTextField(
                  controller: _passwordController,
                  iconData: Icons.lock,
                  hintText: 'Password',
                  isObscure: true,
                ),
                CustomTextField(
                  controller: _confirmPasswordController,
                  iconData: Icons.lock,
                  hintText: 'Confirm Password',
                  isObscure: true,
                ),
              ],
            ),
          ),
          const SizedBox(height: 20),
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: ElevatedButton(
                  child: const Text(
                    'Sign Up',
                    style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                  style: ElevatedButton.styleFrom(
                    primary: Colors.purple,
                    padding: const EdgeInsets.symmetric(vertical: 20),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                  onPressed: _formValidation,
                ),
              ),
            ],
          ),
          const SizedBox(height: 40),
        ],
      ),
    );
  }
}
