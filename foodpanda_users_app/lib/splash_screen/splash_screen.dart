import 'dart:async';
import 'package:flutter/material.dart';
import 'package:foodpanda_users_app/authentication/authentication_page.dart';
import 'package:foodpanda_users_app/global/global.dart';
import 'package:foodpanda_users_app/main/home_page.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  startTimer() async {
    Timer(const Duration(seconds: 4), () async {
      final g = await Global.getInstance();
      Widget page = g.firebaseAuth.currentUser == null ? const AuthenticationPage() : const HomePage();
      Navigator.push(context, MaterialPageRoute(builder: (c) => page));
    });
  }

  @override
  void initState() {
    super.initState();

    startTimer();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Colors.amber,
                Colors.cyan,
              ],
              begin: FractionalOffset(0, 0),
              end: FractionalOffset(1.0, 0.0),
              stops: [0.0, 1.0],
              tileMode: TileMode.clamp,
            ),
          ),
          child: FittedBox(
            fit: BoxFit.scaleDown,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(18.0),
                    child: Image.asset('images/welcome.png'),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      "Order Food Online",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 35,
                        fontFamily: 'Train',
                        letterSpacing: 3,
                      ),
                    ),
                  )
                ],
              ),
            ),
          )),
    );
  }
}
