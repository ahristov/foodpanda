import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

SharedPreferences? sharedPreferences;

class Global {
  static Global? _instance;

  static Future<Global> getInstance() async {
    if (_instance == null) {
      _instance = Global._();
      await _instance!._initialize();
    }

    return _instance!;
  }

  late final SharedPreferences _sharedPreferences;
  late final FirebaseFirestore _firebaseFirestore;
  late final FirebaseStorage _firebaseStorage;
  late final FirebaseAuth _firebaseAuth;

  SharedPreferences get sharedPreferences => _sharedPreferences;
  FirebaseFirestore get firebaseFirestore => _firebaseFirestore;
  FirebaseStorage get firebaseStorage => _firebaseStorage;
  FirebaseAuth get firebaseAuth => _firebaseAuth;

  Global._();

  Future _initialize() async {
    _sharedPreferences = await SharedPreferences.getInstance();
    _firebaseFirestore = FirebaseFirestore.instance;
    _firebaseStorage = FirebaseStorage.instance;
    _firebaseAuth = FirebaseAuth.instance;
  }
}
