import 'package:flutter/material.dart';
import 'package:foodpanda_users_app/widgets/progress_bar.dart';

class LoadingDialog extends StatelessWidget {
  final String message;

  const LoadingDialog({
    required this.message,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      key: key,
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const CircularProgressBar(),
          const SizedBox(height: 10),
          Text(message),
        ],
      ),
    );
  }
}
