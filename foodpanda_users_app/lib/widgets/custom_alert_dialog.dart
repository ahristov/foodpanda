import 'package:flutter/material.dart';

class CustomAlertDialog extends StatelessWidget {
  final String message;
  final String buttonText;
  final Color buttonColor;

  const CustomAlertDialog._({
    required this.message,
    required this.buttonText,
    required this.buttonColor,
    Key? key,
  }) : super(key: key);

  const CustomAlertDialog.alert({required String message, Key? key})
      : this._(
          message: message,
          buttonText: 'OK',
          buttonColor: Colors.red,
          key: key,
        );

  const CustomAlertDialog.info({required String message, Key? key})
      : this._(
          message: message,
          buttonText: 'OK',
          buttonColor: Colors.green,
          key: key,
        );

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      key: key,
      content: Text(message),
      actions: [
        ElevatedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Center(
            child: Text(buttonText),
          ),
          style: ElevatedButton.styleFrom(primary: buttonColor),
        )
      ],
    );
  }
}
