# Food Panda

Contains code and notes from studying [Build FoodPanda & Uber Eats Clone App with Admin WEB Portal](https://www.udemy.com/course/build-foodpanda-uber-eats-clone-app-with-admin-web-portal/)

## Client apps

Sellers app created with:

```bash
flutter create --description "FoodPanda Sellers App" --org "com.atanashristov" --project-name "foodpanda_sellers_app" foodpanda_sellers_app
```

Riders app created with:

```bash
flutter create --description "FoodPanda Riders App" --org "com.atanashristov" --project-name "foodpanda_riders_app" foodpanda_riders_app
```

Users app created with:

```bash
flutter create --description "FoodPanda Users App" --org "com.atanashristov" --project-name "foodpanda_users_app" foodpanda_users_app
```

## Setup FlutterFire

Run:

```bash
npm install -g firebase-tools
firebase login
dart pub global activate flutterfire_cli
```

Then from project's directory:

```bash
flutterfire configure
```

You can setup Android, iOS (only on Mac), web. It will also generate file `firebase_options.dart`.

Then in the `main.dart` file make sure to initialize with options:

```dart
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);

  runApp(const MyApp());
}
```

See:

- [Firebase CLI Setup](https://firebase.google.com/docs/cli#windows-npm)
- [Firebase Flutter Initialization](https://firebase.flutter.dev/docs/overview/#initialization)
