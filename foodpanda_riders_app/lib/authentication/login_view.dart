import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:foodpanda_riders_app/global/global.dart';
import 'package:foodpanda_riders_app/main/home_page.dart';
import 'package:foodpanda_riders_app/widgets/custom_alert_dialog.dart';
import 'package:foodpanda_riders_app/widgets/custom_text_field.dart';
import 'package:foodpanda_riders_app/widgets/loading_dialog.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  Future _authenticateAndLogIn() async {
    showDialog(
      context: context,
      builder: (ctx) => const LoadingDialog(message: 'Login to account, please wait...'),
    );

    User? currentUser;
    final g = await Global.getInstance();

    try {
      await g.firebaseAuth
          .signInWithEmailAndPassword(
        email: _emailController.text.trim(),
        password: _passwordController.text.trim(),
      )
          .then((auth) {
        currentUser = auth.user;
      });
    } on FirebaseAuthException catch (error) {
      _onAuthenticateAndSignUpError(error.message);
      return;
    } catch (error) {
      _onAuthenticateAndSignUpError(error);
      return;
    }

    if (currentUser == null) {
      _onAuthenticateAndSignUpError('Error signing up.');
      return;
    }

    try {
      await _onAuthenticateAndSignUpSuccess(currentUser!);
    } catch (error) {
      _onAuthenticateAndSignUpError(error);
      return;
    }
  }

  void _onAuthenticateAndSignUpError(dynamic error) {
    // close loading pop-up
    Navigator.pop(context);
    // show error
    showDialog(
      context: context,
      builder: (ctx) => CustomAlertDialog.alert(message: error.toString()),
    );
  }

  Future _onAuthenticateAndSignUpSuccess(User currentUser) async {
    final g = await Global.getInstance();

    await g.firebaseFirestore.collection('riders').doc(currentUser.uid).get().then((snapshot) {
      if (snapshot.exists) {
        final userData = snapshot.data() ?? {};
        g.sharedPreferences.setString('uid', currentUser.uid);
        g.sharedPreferences.setString('name', userData['riderName']);
        g.sharedPreferences.setString('email', userData['riderEmail']);
        g.sharedPreferences.setString('avatarUrl', userData['riderAvatarUrl']);
        _popAndNavigateToHome();
      } else {
        g.firebaseAuth.signOut();
        _popAndNavigateToHome();
        showDialog(
          context: context,
          builder: (ctx) => const CustomAlertDialog.alert(message: 'Record not found.'),
        );
      }
    });
  }

  void _popAndNavigateToHome() {
    // close loading pop-up
    Navigator.pop(context);

    // redirect to homepage
    Route newRoute = MaterialPageRoute(builder: (ctx) => const HomePage());
    Navigator.pushReplacement(context, newRoute);
  }

  Future _formValidation() async {
    if (_emailController.text.isEmpty) {
      showDialog(
        context: context,
        builder: (ctx) => const CustomAlertDialog.alert(message: 'Please enter email address.'),
      );
      return;
    }

    if (_passwordController.text.isEmpty) {
      showDialog(
        context: context,
        builder: (ctx) => const CustomAlertDialog.alert(message: 'Please enter password.'),
      );
      return;
    }

    await _authenticateAndLogIn();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.all(15),
              child: Image.asset(
                'images/signup.png',
                height: 270,
              ),
            ),
          ),
          Form(
            key: _formKey,
            child: Column(
              children: [
                CustomTextField(controller: _emailController, iconData: Icons.email, hintText: 'Email'),
                CustomTextField(
                  controller: _passwordController,
                  iconData: Icons.lock,
                  hintText: 'Password',
                  isObscure: true,
                ),
              ],
            ),
          ),
          const SizedBox(height: 20),
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: ElevatedButton(
                  child: const Text(
                    'Login',
                    style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                  style: ElevatedButton.styleFrom(
                    primary: Colors.purple,
                    padding: const EdgeInsets.symmetric(vertical: 20),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                  onPressed: _formValidation,
                ),
              ),
            ],
          ),
          const SizedBox(height: 40),
        ],
      ),
    );
  }
}
