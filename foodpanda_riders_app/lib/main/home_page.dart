import 'package:flutter/material.dart';
import 'package:foodpanda_riders_app/authentication/authentication_page.dart';
import 'package:foodpanda_riders_app/global/global.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Future<Global> get _global async => await Global.getInstance();

  // String name = '';

  @override
  void initState() {
    super.initState();
    // This actually works
    // () async {
    //   final g = await Global.getInstance();
    //   setState(() {
    //     name = g.sharedPreferences.getString('name') ?? '';
    //   });
    // }();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Colors.cyan,
                Colors.amber,
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              tileMode: TileMode.clamp,
            ),
          ),
        ),
        title: FutureBuilder<Global>(
          future: _global,
          builder: (BuildContext context, AsyncSnapshot<Global> snapshot) {
            final sellerName = snapshot.hasData ? snapshot.data!.sharedPreferences.getString('name') ?? '' : '';
            return Text(
              sellerName,
              style: const TextStyle(
                fontSize: 40,
                color: Colors.white,
                fontFamily: "Lobster",
              ),
            );
          },
        ),
        centerTitle: true,
      ),
      body: Center(
          child: ElevatedButton(
        child: const Text("Logout"),
        style: ElevatedButton.styleFrom(primary: Colors.cyan),
        onPressed: () async {
          final g = await Global.getInstance();
          await g.firebaseAuth.signOut();
          Navigator.push(context, MaterialPageRoute(builder: (c) => const AuthenticationPage()));
        },
      )),
    );
  }
}
